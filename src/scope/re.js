// Reasignación y redeclaración

// Con var
var firstName; // Undefined
firstName = 'Oscar';
console.log(firstName);

var lastName = 'David'; // declara y asigna
lastName = 'Ana'; // reasigna
console.log(lastName);

var secondName = 'David'; // declara y asigna
var secondName = 'Ana'; // reasigna
console.log(secondName);

// Con let
let fruit = 'Apple'; // declara y asigna
fruit = 'Kiwi'; // reasigna
console.log(fruit);

let fruit = 'Banana';
console.log(fruit);

// Con const
const animal = 'dog'; // declara y asigna
//animal = 'cat'; // reasigna
const animal = 'snake';
console.log(animal);

const vehicles = [];
vehicles.push('car');
console.log(vehicles);

vehicles.pop();
console.log(vehicles);

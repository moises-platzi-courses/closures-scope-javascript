function greetings() {
  let username = 'Oscar';

  function displayUserName() {
    return `Hello ${username}`;
  }
  return displayUserName;
}

const g = greetings();
console.log(g);
console.log(g());
# Closures y Scope en JavaScript

Comprende a la perfección los diferentes alcances que tienen tus variables cuando son declaradas en JavaScript. Aprende los conceptos fundamentales de Scope global y local, cómo declarar variables con const, let y var. 

+ Aplica estos conocimientos en tus proyectos.
+ Identificar el uso de Local Scope, Function Scope y Block Scope
+ Aprende qué es el Scope, Closures y Hoisting

## Contenido del Curso

### Tipos de scope en JavaScript
+ Global Scope
+ Function Scope
+ Block Scope
+ Reasignación y redeclaración
+ Strict Mode

### Closures
+ ¿Qué es un Closure?
+ Practicando Clousures

### Hoisting

### Debugging
